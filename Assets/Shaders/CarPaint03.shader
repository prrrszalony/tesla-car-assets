Shader "Awesome INC/Awesome CarPaint_CubeMix" {
	Properties {
		_Ambient ("Ambient Color", Color) = (1,1,1,0.5)
		_Color ("Main Color", Color) = (1,1,1,0.5)
		_Candy ("Candy Color", Color) = (1,1,1,0.5)
		_Refl ("Refl Color", Color) = (1,1,1,0.5)
		_MainTex ("Texture", 2D) = "white" {}
		_Cube ("Cubemap", CUBE) = "" {}
		_Cube2 ("Cubemap2", CUBE) = "" {}
		_CubeMix ("CubeMix", Float) = 0.5
		_RRO ("Refl Rim Offset", Float) = 0.5
		_R ("Reflection", Float) = 0.9
		_Power ("Power", Float) = 2.0
		_Saturate ("Saturate", Float) = 0.5
	}
	SubShader {
		Tags { "RenderType" = "Opaque" }
		CGPROGRAM
		#pragma surface surf Lambert
		#pragma target 5.0
		struct Input {
		  float2 uv_MainTex;
		  float3 viewDir;
		  float3 worldRefl;
		  float3 worldPos;
		  INTERNAL_DATA
		};
      
		sampler2D _MainTex;
		samplerCUBE _Cube;
		samplerCUBE _Cube2;

		float4 _Color;
		float4 _Ambient;
		float4 _Candy;
		float4 _Refl;
		half _RRO;
		half _R;
		half _Power;
		half _Saturate;
		half _CubeMix;

		float ZPosition = 0;
		float ZWidth = 1;
		float Change = 0;

		float4 Secondary_Color;
		float4 Secondary_Ambient;
		float4 Secondary_Candy;
		float4 Secondary_Refl;
		half Secondary_RRO;
		half Secondary_R;
		half Secondary_Power;
		half Secondary_Saturate;
		half Secondary_CubeMix;

		void surf (Input IN, inout SurfaceOutput o){      
		
			float t = clamp((ZPosition - IN.worldPos.z + sin(IN.worldPos.x*20+IN.worldPos.y*20) )/ZWidth,-1,1)/2+0.5;
			t *= Change;
			
			_Color = lerp(_Color,Secondary_Color,t);
			_Ambient = lerp(_Ambient,Secondary_Ambient,t);
			_Candy = lerp(_Candy,Secondary_Candy,t);
			_Refl = lerp(_Refl,Secondary_Refl,t);
			
			_RRO = lerp(_RRO,Secondary_RRO,t);
			_R = lerp(_R,Secondary_R,t);
			_Power = lerp(_Power,Secondary_Power,t);
			_Saturate = lerp(_Saturate,Secondary_Saturate,t);
			_CubeMix = lerp(_CubeMix,Secondary_CubeMix,t);
		
		
			half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
			o.Albedo  = (2-pow(rim,2)) * tex2D (_MainTex, IN.uv_MainTex).rgb * (  sqrt(rim) * (_Candy - _Color ) + _Color); 
			float3 saturateCb = texCUBE (_Cube, WorldReflectionVector  (IN, o.Normal)).rgb;
			saturateCb = saturateCb * (1-_Saturate) + _Saturate * float3(1,1,1)*(saturateCb.x+saturateCb.y+saturateCb.z)/3;
			float3 CubeColor = saturateCb*(1-_CubeMix) + _CubeMix * texCUBE (_Cube2, WorldReflectionVector  (IN, o.Normal)).rgb;
			o.Emission = 
			tex2D (_MainTex, IN.uv_MainTex) * _Ambient +_Refl * _R * (_RRO +pow(rim,_Power)) *  (tex2D (_MainTex, IN.uv_MainTex).x+1-tex2D (_MainTex, IN.uv_MainTex).w) * pow(CubeColor,1);
		}
	ENDCG
	} 
	Fallback "Diffuse"
}

